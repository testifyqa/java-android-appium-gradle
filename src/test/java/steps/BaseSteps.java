package steps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import pages.BasePage;
import pages.Page;

import java.util.List;

public class BaseSteps extends Page {

    @And("^the welcome screen has been skipped without logging in$")
    public void theWelcomeScreenHasBeenSkippedWithoutLoggingIn() {
        instanceOf(BasePage.class).skipWelcomeScreen();
    }

    @Then("^the Reddit user sees \"([^\"]*)\" in the PageSource$")
    public void i_see_in_the_PageSource(String expectedPageSource) {
        instanceOf(BasePage.class).validatePageSource(expectedPageSource);
    }

    @Then("^the Reddit user sees$")
    public void iSee(List<String> existsInPageSource) {
        instanceOf(BasePage.class).validateMultipleInPageSource(existsInPageSource);
    }

    @Then("^the Reddit user sees the current activity is \"([^\"]*)\"$")
    public void iSeeTheCurrentActivityIs(String currentActivity) {
        instanceOf(BasePage.class).validateCurrentActivity(currentActivity);
    }

    @Then("^the Reddit user sees the current context is \"([^\"]*)\"$")
    public void iSeeTheCurrentContextIs(String currentContext) {
        instanceOf(BasePage.class).validateCurrentContext(currentContext);
    }
}
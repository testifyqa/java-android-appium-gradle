package pages;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import utils.appium.Driver;
import java.util.List;

import static utils.extensions.MobileElementExtensions.*;

public class BasePage extends Page {

    public AndroidDriver driver = Driver.appDriver();

    private static Logger log = LogManager.getLogger(BasePage.class.getName());

    private String getPageSource() { return driver.getPageSource(); }
    private String getCurrentActivity() { return driver.currentActivity(); }
    private String getCurrentContext() { return driver.getContext(); }

    @AndroidFindBy(id = "com.reddit.frontpage:id/welcome_message")
    private AndroidElement welcomeMessage;

    @AndroidFindBy(id = "com.reddit.frontpage:id/skip_text")
    private AndroidElement btnSkipForNow;

    public void appFullyLaunched() {
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.visibilityOf(welcomeMessage));
    }

    public void validatePageSource(String expectedPageSource) {
        Assert.assertTrue(getPageSource().contains(expectedPageSource));
        log.info(":: The text " + expectedPageSource + " is present in the app screen's source.");
    }

    public void validateMultipleInPageSource(List<String> table) {
        for (String row : table) {
            Assert.assertTrue(getPageSource().contains(row));
            log.info("The text " + row + " is in the app screen's source.");
        }
    }

    public void validateCurrentActivity(String currentActivity) {
        Assert.assertTrue(getCurrentActivity().contains(currentActivity));
        log.info(":: The current activity in the foreground is: " + currentActivity);
    }

    public void validateCurrentContext(String currentContext) {
        Assert.assertTrue(getCurrentContext().contains(currentContext));
        log.info(":: The current context is: " + currentContext);
    }

    public PopularTabPage skipWelcomeScreen() {
        meTap(btnSkipForNow);
        log.info(":: We click the 'Skip For Now' button on the Welcome screen");
        return instanceOf(PopularTabPage.class);
    }
}